import VuexORM from '@vuex-orm/core'
import database from '@/store/database'
import VuexORMAxios from '@vuex-orm/plugin-axios'

VuexORM.use(VuexORMAxios);
export const plugins = [
  VuexORM.install(database)
];
