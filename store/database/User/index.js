import { Model } from '@vuex-orm/core'
import Demand from '@/store/database/Demand'

export default class User extends Model {
  static entity = 'users'

  static fields () {
    return {
      id: this.uid(),
      name: this.string(''),
      latitude: this.number(0),
      longitude: this.number(0),
      demands: this.hasMany(Demand, 'user_id')
    }
  }
}
