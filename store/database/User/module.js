import User from '@/store/database/User'

export default {
  actions: {
    async loadUsers() {
      await User.api().get("users");
    },
  },

  getters: {
    allUsers() {
      return User.all();
    },
    authenticatedUser() {
      return User.find(1);
    },
    userById() {
      return (userId) => {
        return User.find(userId);
      }
    }
  },
};
