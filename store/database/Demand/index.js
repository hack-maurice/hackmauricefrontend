import { Model } from '@vuex-orm/core'
import User from '@/store/database/User'

export default class Demand extends Model {
  static entity = 'demands'

  static fields () {
    return {
      id: this.uid(),
      user_id: this.string(null).nullable(),
      type: this.string(''),
      done: this.boolean(false),
      user: this.belongsTo(User, 'user_id')
    }
  }
}
