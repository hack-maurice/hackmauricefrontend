import { Database } from '@vuex-orm/core'

import UserModel from '@/store/database/User'
import UserModule from '@/store/database/User/module'

import DemandModel from '@/store/database/Demand'

const database = new Database();

database.register(UserModel, UserModule);
database.register(DemandModel, {});

export default database
