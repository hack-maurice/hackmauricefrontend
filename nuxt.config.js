import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  env: {
    baseUrl: 'http://hackmaurice.us-east-2.elasticbeanstalk.com'
  },
  pwa: {
    workbox: {
      config: {
        debug: true
      }
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: "Ça pompe-tu?",
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', href: "https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    '~/assets/styles.css'],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuex-orm-axios'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    ['nuxt-gmaps', {
      key: 'AIzaSyAeOc9b_e3FWUq2gjLnErVA8E2doNMlJ6o',
      libraries: ['places'],
    }]
  ],

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: "https://5e3f0eb064c3f60014550f6e.mockapi.io/"
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#2196F3",
          background: "#F5F6FA",
          secondary: "#E1F5FE",
          accent: "#5699FF",
          error: "#FF3D49",
          warning: "#EF6A6E",
          info: "#607D8B",
          success: "#4CAF50"
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
