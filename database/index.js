import { Database } from '@vuex-orm/core'
import User from '@/models/User'
import Demand from '@/models/Demand'

const database = new Database()

database.register(User)
database.register(Demand)

export default database
